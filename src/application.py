# -*- coding: utf-8 -*-
import platform
arch = platform.architecture()[0]
name = "Socializer"
author = "MCV Software"
authorEmail = "info@mcvsoftware.com"
copyright = "Copyright (C) 2016-2022, MCV Software"
description = name+" Is an accessible VK client for Windows."
url = "https://socializer.su"
# The short name will be used for detecting translation files. See languageHandler for more details.
short_name = "socializer"
translators = ["Darya Ratnikova (Russian)", "Manuel Cortez (Spanish)"]
bts_name = "socializer"
bts_access_token = "U29jaWFsaXplcg"
bts_url = "https://issues.manuelcortez.net"
### Update information
update_url = "https://files.mcvsoftware.com/socializer/update/alpha.json"
# Short_id of the last commit, this is set to none here because it will be set manually by the building tools.
version = "2021.09.22"
update_next_version = "4bdba42e"